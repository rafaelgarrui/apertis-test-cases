stages:
  - test
  - generate
  - lava

variables:
  osname: apertis
  release: v2024dev1

image: $CI_REGISTRY/infrastructure/${osname}-docker-images/${release}-testcases-builder

include:
  - /.gitlab-ci/templates-generate-tests.yml

test-renderer:
  stage: test
  tags:
    - lightweight
  script:
    - python3 -m unittest discover -v

render-pages:
  stage: test
  tags:
    - lightweight
  script:
    - ./atc -d ${release}/ --index-page test-cases/
  artifacts:
    paths:
      - ${release}

.fixedfunction:
  variables:
    type: fixedfunction

.iot:
  variables:
    type: iot
    # IoT image jobs rely on passing credentials through the LAVA definition,
    # so make sure that they're all private.
    visibility: '{group: [Collabora]}'

.hmi:
  variables:
    type: hmi

.basesdk:
  variables:
    type: basesdk

.sdk:
  variables:
    type: sdk

.amd64-uefi:
  variables:
    architecture: amd64
    board: uefi

.amd64-sdk:
  variables:
    architecture: amd64
    board: sdk

.armhf-uboot:
  variables:
    architecture: armhf
    board: uboot

.arm64-uboot:
  variables:
    architecture: arm64
    board: uboot

.arm64-rpi64:
  variables:
    architecture: arm64
    board: rpi64

.cloud2edge-vars:
  variables:
    TENANT: dev.apertis.test
    C2E_DEVICE_ID: dev.apertis.test.ci:$CI_COMMIT_REF_SLUG-$CI_PIPELINE_IID-$architecture-$type-$board

.generate-tests-apt:
  extends: .generate-tests
  tags:
    - lightweight
  stage: generate
  variables:
    deployment: apt
    image_name: ${osname}_${release}-${type}-${architecture}-${board}
    osname: apertis
    release: v2024dev1
    base_url: https://images.apertis.org
    image_path: weekly/v2024dev1
    image_buildid: "fakebuildid"

.generate-tests-ostree:
  extends: .generate-tests
  tags:
    - lightweight
  stage: generate
  variables:
    deployment: ostree
    image_name: ${osname}_ostree_${release}-${type}-${architecture}-${board}
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-${board}
    osname: apertis
    release: v2024dev1
    base_url: https://images.apertis.org
    image_path: weekly/v2024dev1
    image_buildid: "fakebuildid"

.run-tests:
  stage: lava
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$LAVA_URL == null'
      when: never
    - if: '$LAVA_TOKEN == null'
      when: never
    - when: on_success

generate-tests-apt-amd64-fixedfunction-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .fixedfunction

run-tests-apt-amd64-fixedfunction-uefi:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-amd64-fixedfunction-uefi
        artifact: child-pipeline.yaml

generate-tests-apt-armhf-fixedfunction-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .fixedfunction

run-tests-apt-armhf-fixedfunction-uboot:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-armhf-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-fixedfunction-uboot:
  extends:
    - .generate-tests-apt
    - .arm64-uboot
    - .fixedfunction

run-tests-apt-arm64-fixedfunction-uboot:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-arm64-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-fixedfunction-rpi64-rpi4:
  extends:
    - .generate-tests-apt
    - .arm64-rpi64
    - .fixedfunction

run-tests-apt-arm64-fixedfunction-rpi64-rpi4:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-arm64-fixedfunction-rpi64-rpi4
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-iot-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .iot

run-tests-apt-amd64-iot-uefi:
  extends:
    - .run-tests
    - .amd64-uefi
    - .iot
    - .cloud2edge-vars
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-amd64-iot-uefi
        artifact: child-pipeline.yaml

generate-tests-apt-armhf-iot-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .iot

run-tests-apt-armhf-iot-uboot:
  extends:
    - .run-tests
    - .armhf-uboot
    - .iot
    - .cloud2edge-vars
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-armhf-iot-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-iot-uboot:
  extends:
    - .generate-tests-apt
    - .arm64-uboot
    - .iot

run-tests-apt-arm64-iot-uboot:
  extends:
    - .run-tests
    - .arm64-uboot
    - .iot
    - .cloud2edge-vars
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-arm64-iot-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-arm64-iot-rpi64-rpi4:
  extends:
    - .generate-tests-apt
    - .arm64-rpi64
    - .iot

run-tests-apt-arm64-iot-rpi64-rpi4:
  extends:
    - .run-tests
    - .arm64-rpi64
    - .iot
    - .cloud2edge-vars
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-arm64-iot-rpi64-rpi4
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-hmi-uefi:
  extends:
    - .generate-tests-apt
    - .amd64-uefi
    - .hmi

run-tests-apt-amd64-hmi-uefi:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-amd64-hmi-uefi
        artifact: child-pipeline.yaml

generate-tests-apt-armhf-hmi-uboot:
  extends:
    - .generate-tests-apt
    - .armhf-uboot
    - .hmi

run-tests-apt-armhf-hmi-uboot:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-armhf-hmi-uboot
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-basesdk-sdk:
  extends:
    - .generate-tests-apt
    - .amd64-sdk
    - .basesdk

run-tests-apt-amd64-basesdk-sdk:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-amd64-basesdk-sdk
        artifact: child-pipeline.yaml

generate-tests-apt-amd64-sdk-sdk:
  extends:
    - .generate-tests-apt
    - .amd64-sdk
    - .sdk
run-tests-apt-amd64-sdk-sdk:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-apt-amd64-sdk-sdk
        artifact: child-pipeline.yaml

generate-tests-ostree-amd64-fixedfunction-uefi:
  extends:
    - .generate-tests-ostree
    - .amd64-uefi
    - .fixedfunction

run-tests-ostree-amd64-fixedfunction-uefi:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-ostree-amd64-fixedfunction-uefi
        artifact: child-pipeline.yaml

generate-tests-ostree-armhf-fixedfunction-uboot:
  extends:
    - .generate-tests-ostree
    - .armhf-uboot
    - .fixedfunction

run-tests-ostree-armhf-fixedfunction-uboot:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-ostree-armhf-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-ostree-arm64-fixedfunction-uboot:
  extends:
    - .generate-tests-ostree
    - .arm64-uboot
    - .fixedfunction

run-tests-ostree-arm64-fixedfunction-uboot:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-ostree-arm64-fixedfunction-uboot
        artifact: child-pipeline.yaml

generate-tests-ostree-arm64-fixedfunction-rpi64-rpi4:
  extends:
    - .generate-tests-ostree
    - .arm64-rpi64
    - .fixedfunction
  variables:
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-uboot

run-tests-ostree-arm64-fixedfunction-rpi64-rpi4:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-ostree-arm64-fixedfunction-rpi64-rpi4
        artifact: child-pipeline.yaml

generate-tests-ostree-amd64-hmi-uefi:
  extends:
    - .generate-tests-ostree
    - .amd64-uefi
    - .hmi

run-tests-ostree-amd64-hmi-uefi:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-ostree-amd64-hmi-uefi
        artifact: child-pipeline.yaml

generate-tests-ostree-arm64-hmi-rpi64-rpi4:
  extends:
    - .generate-tests-ostree
    - .arm64-rpi64
    - .hmi
  variables:
    image_bundle: ${osname}_ostree_${release}-${type}-${architecture}-uboot

run-tests-ostree-arm64-hmi-rpi64-rpi4:
  extends: .run-tests
  trigger:
    strategy: depend
    include:
      - job: generate-tests-ostree-arm64-hmi-rpi64-rpi4
        artifact: child-pipeline.yaml
