metadata:
  name: apparmor-bluez-setup
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: high
  maintainer: "Apertis Project"
  description: "Test AppArmor profiles for setting up of Bluetooth stack by
                running basic tests using hciconfig. Check if the interface can
                be brought up/down, can be changed of mode and can discover
                devices."

  resources:
    - "A Bluetooth adapter."
    - "A Bluetooth device."

  macro_ostree_preconditions:
    - reponame: apparmor-bluez-setup
      branch: apertis/v2024dev1
    - reponame: bluez-setup
      branch: apertis/v2024dev1
  pre-conditions:
    - "Please note that connman disables bluetooth by default on a fresh image.
       If it's already enabled, connmanctl will give an \"In progress\" error that
       may be ignored. To enable the device:"
    - $ connmanctl enable bluetooth

  expected:
    - "The output of check-setup.sh should be similar to that:"
    - |
        >hci0:   Type: BR/EDR  Bus: USB
        BD Address: 00:1B:DC:06:05:D4  ACL MTU: 310:10  SCO MTU: 64:8
        UP RUNNING PSCAN
        RX bytes:16617 acl:0 sco:0 events:630 errors:0
        TX bytes:7102 acl:0 sco:0 commands:629 errors:8
        Features: 0xff 0xff 0x8f 0x7e 0xd8 0x1f 0x5b 0x87
        Packet type: DM1 DM3 DM5 DH1 DH3 DH5 HV1 HV2 HV3
        Link policy: RSWITCH HOLD SNIFF PARK
        Link mode: SLAVE ACCEPT
        Name: 'sac-target-0'
        Class: 0x620100
        Service Classes: Networking, Audio, Telephony
        Device Class: Computer, Uncategorized
        HCI Version: 4.0 (0x6)  Revision: 0x2031
        LMP Version: 4.0 (0x6)  Subversion: 0x2031
        Manufacturer: Cambridge Silicon Radio (10)

        Put some devices around in discoverable mode and press ENTER

        Scanning ...
               78:47:1D:B3:6E:80       jasmin-0
        Check if the devices you enabled are in the list above
    - "When the TC has been run, once the log collection script finishes check
       its output."
    - "On Success, aa_get_complaints.sh will find no complaints:"
    - |
        >[...snip useless output...]
        >>> Checking for apparmor complaints ...
        >>> No complaints found!
    - "If something goes wrong, the output will be similar to:"
    - |
        >[...snip useless output...]
        >>> Complaints found, creating report ...
        aa-dump_20180710-100931/
        aa-dump_20180710-100931/complaint_tokens.log
        aa-dump_20180710-100931/audit.log
        aa-dump_20180710-100931/ps_aux.log
        aa-dump_20180710-100931/uname.log
        aa-dump_20180710-100931/journalctl.log
        aa-dump_20180710-100931/image_version
        aa-dump_20180710-100931/os-release
        >>> Report created as /home/chaiwala/aa-dump_20180710-100931.tar.bz2
    - "In this case file a bug report against AppArmor attaching the tar.bz2 file
       created."

run:
  steps:
    - "First ensure that the bluez-setup test case is passing before executing the
       apparmor-bluez-setup test one (otherwise false-negative might occur):"
    - $ cd $HOME/bluez-setup-* ; ./check-setup.sh
    - "After verifying that the bluez-setup test is passing, open a new terminal
       and run the following. Do not yet answer to the question."
    - $ cd $HOME/apparmor-bluez-setup-* ; ./aa_get_complaints.sh
    - "Now, run the bluez-setup test case again in the previous terminal."
    - $ cd $HOME/bluez-setup-* ; ./check-setup.sh
    - "After the bluez-setup test ended, answer y to check for complaints."
