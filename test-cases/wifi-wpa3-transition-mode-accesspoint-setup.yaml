metadata:
  name: wifi-wpa3-transition-mode-accesspoint-setup
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi: [ armhf ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: critical
  maintainer: "Apertis Project"
  description: "Test WiFi WPA3-Transition-Mode access point (AP mode) is working in Apertis. Also check connection is re-established after rebooting the AP."

  resources:
    - "Two Wi-Fi USB dongles, https://www.apertis.org/reference_hardware/extras/"

  pre-conditions:
    - "Make sure to connect the Wi-Fi USB dongles to both target and SDK."

  expected:
    - "After setting up the access point, you should be able to connect from the
      SDK to the Wi-Fi AP 'APERTIS_AP' running on the target device."
    - "After rebooting the target device, you should be able to reconnect from
      the SDK to the Wi-Fi access point 'APERTIS_AP' running on the target device."
    - "On the SDK, ensure to be connected to the 'APERTIS_AP' network"
    - |
        $> connmanctl> services
        *AR APERTIS_AP     wifi_1c872cc7c71d_4b45594d4e474d545f50534b_managed_psk
        connmanctl>
    - "On the target device, the 'hostapd' output log should show a successful connection:"
    - |
        >$ journalctl -b -u hostapd
        wlan0: AP-STA-CONNECTED 0c:9d:92:70:fd:4c
        wlan0: STA 0c:9d:92:70:fd:4c RADIUS: starting accounting session 97DB26400B1D3921
        wlan0: AP-STA-DISCONNECTED 0c:9d:92:70:fd:4c
        wlan0: STA 0c:9d:92:70:fd:4c IEEE 802.11: authenticated
        wlan0: STA 0c:9d:92:70:fd:4c IEEE 802.11: associated (aid 1)
        wlan0: AP-STA-CONNECTED 0c:9d:92:70:fd:4c
        wlan0: STA 0c:9d:92:70:fd:4c RADIUS: starting accounting session AEBCF08534CAE0B6
        wlan0: AP-STA-DISCONNECTED 0c:9d:92:70:fd:4c
        wlan0: interface state ENABLED->DISABLED
        wlan0: AP-DISABLED
        wlan0: CTRL-EVENT-TERMINATING
        nl80211: deinit ifname=wlx1c872cc7c71d disabled_11b_rates=0

run:
  steps:
    - "On the target device execute the below steps."
    - "Set configuration parameters for this test case (might be previously defined by tests based on this one):"
    - $ WIFI_KEYMGMT="${WIFI_KEYMGMT:-WPA-PSK SAE}"
    - $ WIFI_80211W="${WIFI_80211W:-1}"
    - "Check the wifi interface name (e.g. 'wlan0') running:"
    - $ connmanctl enable wifi
    - $ WIFI_SERVICE=$(connmanctl services | grep -m 1 -o 'wifi_.*')
    - $ WIFI_IFACE=$(connmanctl services "${WIFI_SERVICE}" | grep -o 'Interface=\w\+' | sed s/Interface=//)
    - "Create the file '/etc/hostapd/hostapd.conf' with the following content:"
    - |
        >$ sudo tee /etc/hostapd/hostapd.conf << EOF
        interface=${WIFI_IFACE:?}
        wpa_key_mgmt=${WIFI_KEYMGMT:?}
        ieee80211w=${WIFI_80211W:?}
        ssid=APERTIS_AP
        auth_algs=1
        wpa=2
        wpa_passphrase=12345678
        sae_password=12345678
        channel=11
        ctrl_interface=/var/run/hostapd
        driver=nl80211
        ht_capab=[SHORT-GI-20]
        hw_mode=g
        ieee80211ac=1
        ieee80211n=1
        rsn_pairwise=CCMP
        sae_require_mfp=1
        vht_oper_chwidth=0
        wpa_pairwise=CCMP
        EOF
    - "Create the file '/lib/systemd/network/hostapd.network' with the following content:"
    - |
        >$ sudo tee /lib/systemd/network/hostapd.network << EOF
        [Match]
        Name=${WIFI_IFACE:?}
        [Network]
        Address=192.168.72.1/24
        DHCPServer=yes
        EOF
    - "Finally, run the following commands on the target device:"
    - "$ sudo systemctl stop connman"
    - "$ sudo rfkill unblock wifi"
    - "$ sudo modprobe mac80211"
    - "$ sudo iptables --flush"
    - "$ sudo systemctl restart systemd-networkd"
    - "$ sudo networkctl up ${WIFI_IFACE:?}"
    - "$ sudo systemctl unmask hostapd"
    - "$ sudo systemctl restart hostapd"
    - "On the SDK, connect to 'APERTIS_AP' running the following commands:"
    - |
        >$ connmanctl
        connmanctl> enable wifi
        connmanctl> agent on
        connmanctl> scan wifi
        Scan completed for wifi
        connmanctl> services
        APERTIS_AP         wifi_1c872cc7c71d_4b45594d4e474d545f50534b_managed_psk
        connmanctl> connect wifi_1c872cc7c71d_4b45594d4e474d545f50534b_managed_psk
        Agent RequestInput wifi_1c872cc7c71d_4b45594d4e474d545f50534b_managed_psk
        Passphrase = [ Type=psk, Requirement=mandatory ]
        Passphrase? 12345678
        Connected wifi_1c872cc7c71d_4b45594d4e474d545f50534b_managed_psk
        connmanctl> services
        *AR APERTIS_AP     wifi_1c872cc7c71d_4b45594d4e474d545f50534b_managed_psk
        connmanctl>
    - "After the connection has been established, reboot the access point by
      pressing the reboot button on the target device. Then, on the target device,
      restart the 'hostapd' daemon and 'systemd-networkd' service, by running:"
    - "$ sudo systemctl restart systemd-networkd"
    - "$ sudo systemctl restart hostapd"
    - "On the SDK, reconnect to 'APERTIS_AP' by re-running the previous
      'connmanctl' commands."
