metadata:
  name: bluez-hfp
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test Hands Free Profile (HFP) BlueZ feature."

  resources:
    - "A Bluetooth adapter"
    - "A Bluetooth-enabled phone with a SIM card and a data plan. (https://www.apertis.org/reference_hardware/extras/)"

  macro_ostree_preconditions:
    reponame: bluez-phone
    branch: apertis/v2024dev1

  pre-conditions:
    - "Please note that connman disables bluetooth by default on a fresh image."
    - "Enable device:"
    - "$ connmanctl enable bluetooth"
    - "When running the test on SabreLite boards using the headphone jack to
       check the audio output, the audio manager needs to be configured to route it
       accordingly as it defaults to the HDMI output:"
    - $ pactl set-default-sink alsa_output.platform-sound.stereo-fallback
    - $ pactl set-sink-port alsa_output.platform-sound.stereo-fallback analog-output-headphones
    - "After reboot ensure that headphones will not be muted:"
    - $ pactl set-sink-mute alsa_output.platform-sound.stereo-fallback false


  expected:
    - "If PASSED is displayed, all of the test have passed. The output should be
       similar to that:"
    - |
        >select_adapter
         Selected /org/bluez/hci0
         hci0
         select_device: Discovering...
         (According to introspection data, you need to pass 'ss')
         Device found: 20:34:FB:8B:C6:06 <unnamed>
         Device found: 30:C3:D9:90:C2:43 ICC6.5in
         Device found: 20:34:FB:6E:2F:FC pairing
         Input device address: 20:34:FB:6E:2F:FC
         Selected address: 20:34:FB:6E:2F:FC
         test_pairing_initiator
         Master: hci0
         Slave: 20:34:FB:6E:2F:FC (bdaddr)
         Master address: 00:02:72:A7:C3:03
         Slave address: 20:34:FB:6E:2F:FC
         Matching 
         type='signal',interface='org.freedesktop.DBus.ObjectManager',member='Interfaces
         Added',path=/,
         Matching 
         type='signal',interface='org.freedesktop.DBus.Properties',member='PropertiesCha
         nged',arg0='org.bluez.Device1'
         Scanning
         Property changed: org.bluez.Device1
         Property: RSSI
         Property changed: org.bluez.Device1
            Property: RSSI
            Property: Name
            Property: Alias
            Property: UUIDs
         Device added: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Pairing requested: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Property changed: org.bluez.Device1
            Property: Connected
         RequestConfirmation (/org/bluez/hci0/dev_20_34_FB_6E_2F_FC, 696478)
         Device added: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Property changed: org.bluez.Device1
            Property: Modalias
            Property: UUIDs
            Property: ServicesResolved
            Property: Paired=1
         Successfully paired to /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Done
         Device found: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Device 20_34_FB_6E_2F_FC is paired
         test_pairing_responder
         Start a pairing from the phone 20:34:FB:6E:2F:FC!
         Master: hci0
         Slave: 20:34:FB:6E:2F:FC (bdaddr)
         Master address: 00:02:72:A7:C3:03
         Slave address: 20:34:FB:6E:2F:FC
         Matching 
         type='signal',interface='org.freedesktop.DBus.ObjectManager',member='Interfaces
         Added',path=/,
         Matching 
         type='signal',interface='org.freedesktop.DBus.Properties',member='PropertiesChanged',arg0='org.bluez.Device1'
         Pairable: True
         Discoverable: False
         Waiting for pairing
         Device added: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Property changed: org.bluez.Device1
         Property: UUIDs
         Device added: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Property changed: org.bluez.Device1
            Property: Modalias
            Property: UUIDs
            Property: ServicesResolved
            Property: Paired=1
         Successfully paired to /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Done
         Device found: /org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         Device 20_34_FB_6E_2F_FC is paired
         test_profiles
         OBEXObjectPush
         AudioSource
         AV Remote Control Target
         Unknown profile '0000110d-0000-1000-8000-00805f9b34fb',
         AV Remote Control
         Headset Audio Gateway
         PANU
         NAP
         Handsfree Audio Gateway
         test_profile_hfp_src
         select_objectpath_hfp
         /org/bluez/hci0
         /hfp/org/bluez/hci0
         '/hfp/org/bluez/hci0/dev_20_34_FB_6E_2F_FC',
         object path going/hfp/org/bluez/hci0/dev_20_34_FB_6E_2F_FC
         $OBJECTPATHHFP is not empty
         Type the phone number to call:7798433607
         Dialing to the phone number: 7798433607
         Calling .......
         CALL_PATH /hfp/org/bluez/hci0/dev_20_34_FB_6E_2F_FC/voicecall01
         Did you recieved the call (y/n):
         y
         Hanging up the call
         ()
         From a second phone call the phone connected to oFono
         ([(objectpath '/hfp/org/bluez/hci0/dev_20_34_FB_6E_2F_FC/voicecall01', {'State': <'incoming'>, 'LineIdentification': <'+918618923989'>, 'Name': <''>, 'Multiparty': <false>, 'RemoteHeld': <false>, 'RemoteMultiparty': <false>, 'Emergency': <false>})],)
         Call incoming
         Did you hear the call (y/n):
         y
         Hanging up the call
         ()
         SimAccess
         PBAP Phonebook Access PSE
         MAP Message Access Server
         PnPInformation
         Unknown profile '00001800-0000-1000-8000-00805f9b34fb',
         Unknown profile '00001801-0000-1000-8000-00805f9b34fb']>,)
         PASSED
    - "If FAILED is displayed, bluez-hfp test has failed and the test sequence
       is interrupted."

  notes:
    - "Errors most frequently occurs at pairing. This can be used to circumvent
       pairing problems, though this does not mean the test is a full pass, as
       the pairing is part of the test:"
    - |
        >cd ~/bluez-phone-master
        armhf/bin/pair_two hci0 <bt address of the phone>
        ./bluez-hfp.sh -a <address of the phone> -s

run:
  steps:
    - "The phone must be in discoverable mode for starting this test. Look for
       the phone in this list, and save the BT address. It will be used while
       running the test."
    - "Execute the test suite inside an environment with dbus:"
    - $ ./bluez-hfp.sh
    - "There are some options:"
    - |
        >-a select which device you want to pair with (specify the address)
        -s skip pairing because the device is already paired. -a must be present when using this.
        -x enables test debugging (only useful if a test fails)
    - "Once the test begins, after Apertis finishes pairing with the phone, you
       must initiate pairing from the phone and do the pairing again as part of
       the test. You may need to unpair Apertis from within the phone first. The
       test will display the following message when that is required:"
    - |
        >Start a pairing from the phone! was it successful (y/n).
    - "After you've initiated the pairing from the phone, the test will continue."
    - "If the pairing fails from the test, try to do the pairing separately, and
       then run the test using './bluez_hfp.sh -a <phone addr> -s'. To do a
       separate pairing: unpair the phone, and run 'bluetoothctl'. Then, in
       bluetoothctl prompt, issue the following commands in order: 'remove
       <phone addr>', 'discoverable on', 'agent off', 'agent NoInputNoOutput',
       'default-agent'. The either 'pairable on' to pair from phone, or 'pair
       <phone addr>' to issue a pairing to the phone. '<phone addr>' is the phone
       address of the form 'a1:b2:c3:d4:e5:f6'."
    - "The next step is to initiate a call from the phone. This can be
       done by entering the contact. The test will display the
       following message when that is required:"
    - |
        >Did you recieved the call (y/n)
    - "After you’ve recieved the call in other phone number
       continue."
    - "The next step is to call from other phone to the phone connected to the 
       bluetooth adapter. The test will display the
       following message when that is required:"
    - |
        >Did you hear the call (y/n):
    - "After you’ve heard the call from other phone number
       the test case is passed"
